package JavaBase;

public class Circle {

    private double rasius;

    public Circle()
    {
        this.rasius=1;
    }
    public Circle(double rasius)
    {
        this.rasius=rasius;
    }
    public double getRasius()
    {
        return rasius;
    }
    public void setRasius(double rasius)
    {
        this.rasius=rasius;
    }
    public double getArea()
    {
        double p=3.14;
        if(rasius<=0)
        {
            System.out.println("半径无意义");
            System.exit(1);
        }
            return p*this.rasius*this.rasius;
    }
    public double getPerimeter()
    {
        double p=3.14;
        if(rasius<=0)
        {
            System.out.println("半径无意义");
            System.exit(1);
        }
        return 2*p*this.rasius;
    }
    public void show()
    {
        System.out.println("圆的半径： "+this.rasius);
        System.out.println("圆的周长： "+getPerimeter());
        System.out.println("圆的面积： "+getArea());
    }

}
