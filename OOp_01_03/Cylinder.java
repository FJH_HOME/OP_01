package JavaBase;

public class Cylinder extends Circle{
    private double hight;
    public Cylinder(double rasius,double hight)
    {
        super(rasius);
        this.hight=hight;
    }
    public double getHight()
    {
        return hight;
    }
    public void setHight(double hight)
    {
        this.hight=hight;
    }
    public double getVolume()
    {
        return super.getArea()*this.hight;
    }
    public void showVolume()
    {
         System.out.println("圆柱体的体积： "+getVolume());
    }


}
