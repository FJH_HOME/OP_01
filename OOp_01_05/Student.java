package JavaBase;

public class Student {
    private String name;
    private int age;
    public Student()
    {
        this.name="小红";
        this.age=25;
    }
    public Student(String name,int age)
    {
        this.name=name;
        this.age=age;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name=name;
    }
    public int getAge()
    {
        return age;
    }
    public void setAge(int age)
    {
        if(age<0||age>150)
        {
            System.out.println("******输入的年龄为："+age+",该年龄不合法！重置******");
            return;
        }
        this.age=age;
    }
    public void say()
    {
        System.out.println("学生姓名："+this.name);
        System.out.println("年龄："+this.age);
    }
}
