package JavaBase;

public class Calculator {

    public double plus(double num1,double num2)
    {
        return num1+num2;
    }
    public double subtract(double num1,double num2)
    {
        return num1-num2;
    }
    public double multiply(double num1,double num2)
    {
        return num1*num2;
    }
    public double divide(double num1,double num2)
    {
        if(num2==0)
        {
            System.out.println("除数不能为0");
            System.exit(1);
        }
        return num1/num2;
    }

}
