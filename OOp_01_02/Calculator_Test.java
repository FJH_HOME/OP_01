package JavaBase;

import java.util.Scanner;

public class Calculator_Test {
    public static void main(String args[])
    {
        Scanner input=new Scanner(System.in);
        Calculator calculator=new Calculator();
        System.out.print("输入第一个数：");
        double num1=input.nextDouble();
        System.out.print("输入第二个数：");
        double num2=input.nextDouble();
        System.out.println(num1+"+"+num2+"="+calculator.plus(num1,num2));
        System.out.println(num1+"-"+num2+"="+calculator.subtract(num1,num2));
        System.out.println(num1+"*"+num2+"="+calculator.multiply(num1,num2));
        System.out.println(num1+"/"+num2+"="+calculator.divide(num1,num2));
    }
}
