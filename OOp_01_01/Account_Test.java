package JavaBase;

import java.util.ArrayList;
import java.util.List;

public class Account_Test {
    public static List<Account> AccountList=new ArrayList<Account>();
    public static void initData()
    {
        AccountList.add(new Account("fj2132",123456,"张三","11111111","123@qq.com",1246));
        AccountList.add(new Account("yh2357",123456,"李四","22222222","345@qq.com",6546));
        AccountList.add(new Account("sd3467",123456,"王五","33333333","765@qq.com",76543));
        AccountList.add(new Account("xd8765",123456,"赵六","44444444","987@qq.com",12345));
    }
    public static void main(String args[])
    {
        initData();//初始化账户信息
        for(int i=0;i<AccountList.size();i++)
        {
            AccountList.get(i).test();
            System.out.println();
            AccountList.get(i).deposit(1000);
            System.out.println();
            AccountList.get(i).withdraw(500);
            System.out.println();
        }


    }
}
