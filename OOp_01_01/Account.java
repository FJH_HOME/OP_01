package JavaBase;

public class Account {
    public String id;//账户号码
    public int password;//账户密码
    public String name;//真实姓名
    public String personId;//身份证号码
    public String email;//电子邮件
    public double balance;//余额

    public  Account()
    {
        this.id="de233";
        this.password=123;
        this.name="十年";
        this.personId="12343433";
        this.email="1234@qq.com";
        this.balance=1234;

    }
    public Account(String id,int password,String name,String personId,String email,double balance)
    {
        this.id=id;
        this.password=password;
        this.name=name;
        this.personId=personId;
        this.email=email;
        this.balance=balance;
    }
    public void test()
    {
        System.out.println("账户号码： "+this.id);
        System.out.println("账户密码： "+this.password);
        System.out.println("真实姓名： "+this.name);
        System.out.println("身份证号码： "+this.personId);
        System.out.println("电子邮箱： "+this.email);
        System.out.println("账号余额： "+this.balance);
    }
    public  void deposit(double balance)
    {
        this.balance+=balance;
        System.out.println("存款金额： "+balance);
        System.out.println("账户余额： "+this.balance);
    }
    public void withdraw(double balance)
    {
        if(this.balance<balance)
        {
            System.out.println("账号余额不足！");
        }
        else
        {
            this.balance-=balance;
            System.out.println("取款金额： "+balance);
            System.out.println("账户余额： "+this.balance);
        }
    }

}
