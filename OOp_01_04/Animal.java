package JavaBase;

public class Animal {
    private String name;
    public Animal(String name)
    {
        this.name=name;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name=name;
    }
    public void printShow()
    {
        System.out.println("名字："+this.name);
    }
}
