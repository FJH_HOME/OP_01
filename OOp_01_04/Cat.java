package JavaBase;

public class Cat extends Animal{
    private String hairColor;
    public Cat(String name,String hairColor)
    {
        super(name);
        this.hairColor=hairColor;
    }
    public void printShow()
    {
        System.out.print("猫咪");
        super.printShow();
        System.out.println("毛色："+hairColor);
    }
    public void catchMice()
    {
        System.out.println("本领：抓老鼠");
    }

}
